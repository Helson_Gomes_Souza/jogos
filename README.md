# Blackjack (Jogo 21) em Python

O arquivo blackjack.py demonstra de maneira simplificada a representação do jogo
21 em python e como um iniciante de programação em Python poderia proceder para
criar um jogo nesse estilo.

Os códigos não utilizam bibliotecas específicas para criação de jogos. A estrutura
da programação é composta por funções básicas e simples que não exigem conhecimento
elevado de prograação.

##Regras

O jogador inicia com um cachê de R$ 1.000,00 na sua carteira e pode apostar qualquer valor
disponível em caixa. O jogo encerra se o jogador perde tudo. Tanto a banca
quanto o jogador recebem três cartas (uma a uma) do baralho e podem pedir mais três
cartas caso a sua pontuação não ultrapasse 21 pontos.

O baralho possui 52 cartas, sendo quatro cartas de cada espécie (As, dois,
três, quatro, cinco, seis, sete, oito, nove, dez, valete, dama e reis).
Como os naipes não importam no blackjack, a representação das cartas não
possui naipes.

O jogador ganha da banca caso:
1. Faça 21 pontos e a banca faça uma pontuação diferente de 21.
2. Faça menos de 21 pontos e a banca faça menos pontos que o jogador.
3. Faça menos de 21 pontos e a banca faça mais de 21 pontos.

O jogador nem ganha nem perde caso:
1. Faça a mesma pontuação da banca.
2. Tanto o jogador quanto a banca façam mais de 21 pontos.

Em ocasiões contrárias à essas condições, o jogador perde o valor que apostou
e esse valor é descontado da sua carteira.
