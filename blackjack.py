print('----------------------------------------------------------------------------------------------------------')
print('')
print(' *           *           **********  **********  *        *            *  **********  *********  *       * ')
print(' *           *           *        *  *           *      *              *  *        *  *          *     *   ')
print(' *           *           *        *  *           *    *                *  *        *  *          *   *     ')
print(' *********   *           **********  *           *  *                  *  **********  *          * *       ')
print(' *       *   *           *        *  *           *    *        *       *  *        *  *          *   *     ')
print(' *       *   *           *        *  *           *      *      *       *  *        *  *          *     *   ')
print(' *********   **********  *        *  **********  *        *    *********  *        *  *********  *        * ')
print('----------------------------------------------------------------------------------------------------------')

cartas = list(range(1, 53))

ases = list(range(1,5))
dois = list(range(5,9))
tres = list(range(9,13))
quatro = list(range(13,17))
cinco = list(range(17, 21))
seis = list(range(21, 25))
sete = list(range(25,29))
oito = list(range(29, 33))
nove = list(range(33,37))
dez = list(range(37,41))
valete = list(range(41, 45))
damas = list(range(45,49))
reis = list(range(49, 53))

# Desenhando o baralho
carta_as = (' _____________ \n|             |\n|             |\n|      .      |\n|     . .     |\n|    .   .    |\n|   . ... .   |\n|  .       .  |\n| ...     ... |\n|_____________|')
carta_dois = (' _____________ \n|             |\n|  ........   |\n|         .   |\n|         .   |\n|  ........   |\n|  .          |\n|  .          |\n|  ........   |\n|_____________|')
carta_tres = (' _____________ \n|             |\n|  ........   |\n|         .   |\n|         .   |\n|  ........   |\n|         .   |\n|         .   |\n|  ........   |\n|_____________|')
carta_quatro = (' _____________ \n|             |\n|  .      .   |\n|  .      .   |\n|  .      .   |\n|  ........   |\n|         .   |\n|         .   |\n|         .   |\n|_____________|')
carta_cinco = (' _____________ \n|             |\n|  ........   |\n|  .          |\n|  .          |\n|  ........   |\n|         .   |\n|         .   |\n|  ........   |\n|_____________|')

carta_seis = (' _____________ \n|             |\n|  ........   |\n|  .          |\n|  .          |\n|  . ......   |\n|  .      .   |\n|  .      .   |\n|  ........   |\n|_____________|')

carta_sete = (' _____________ \n|             |\n|   ........  |\n|         .   |\n|        .    |\n|    .....    |\n|     .       |\n|    .        |\n|   .         |\n|_____________|')

carta_oito = (' _____________ \n|             |\n|  .........  |\n|  .       .  |\n|  .       .  |\n|  .........  |\n|  .       .  |\n|  .       .  |\n|  .........  |\n|_____________|')

carta_nove = (' _____________ \n|             |\n|  .........  |\n|  .       .  |\n|  .       .  |\n|  .........  |\n|          .  |\n|          .  |\n|  .........  |\n|_____________|')

carta_dez = (' _____________ \n|             |\n| .  .......  |\n| .  .     .  |\n| .  .     .  |\n| .  .     .  |\n| .  .     .  |\n| .  .     .  |\n| .  .......  |\n|_____________|')

carta_valete = (' _____________ \n|             |\n|  ........   |\n|         .   |\n|         .   |\n|    .    .   |\n|   .     .   |\n|  .      .   |\n|  ........   |\n|_____________|')

carta_dama = (' _____________ \n|             |\n| ..........  |\n| .        .  |\n| .        .  |\n| .    .   .  |\n| .   .    .  |\n| ..........  |\n|    .        |\n|_____________|')
carta_reis = (' _____________\n|             |\n|   .      .  |\n|   .    .    |\n|   .  .      |\n|   . .       |\n|   .   .     |\n|   .     .   |\n|   .       . |\n|_____________|')



pontos = []
pontos_banca = []
inicio = 1000
print('\n \n Você iniciará o jogo com R$ 1000,00 \n \n ')
n = 0

# Sortei das cartas para o jogador
import random
import time
while n >= 0:
    aposta = float(input('Digite o valor da sua aposta: \n'))
    if inicio <= 0 or inicio < aposta:
        print('\n Você não tem saldo para fazer esta aposta! \n')
        break
    sorteio = list(random.sample(cartas, 3))
    print('\n ------------------- \n As suas cartas são: \n -------------------')
    for i in range(0, 3):
        if sorteio[i] in ases:
            if sum(pontos) < 11:
                print(carta_as)
                pontos.append(11)
            else:
                print(carta_as)
                pontos.append(1)
        if sorteio[i] in dois:
            print(carta_dois)
            pontos.append(2)
        if sorteio[i] in tres:
            print(carta_tres)
            pontos.append(3)
        if sorteio[i] in quatro:
            print(carta_quatro)
            pontos.append(4)
        if sorteio[i] in cinco:
            print(carta_cinco)
            pontos.append(5)
        if sorteio[i] in seis:
            print(carta_seis)
            pontos.append(6)
        if sorteio[i] in sete:
            print(carta_sete)
            pontos.append(7)
        if sorteio[i] in oito:
            print(carta_oito)
            pontos.append(8)
        if sorteio[i] in nove:
            print(carta_nove)
            pontos.append(9)
        if sorteio[i] in dez:
            print(carta_dez)
            pontos.append(10)
        if sorteio[i] in valete:
            print(carta_valete)
            pontos.append(10)
        if sorteio[i] in damas:
            print(carta_dama)
            pontos.append(10)
        if sorteio[i] in reis:
            print(carta_reis)
            pontos.append(10)
        time.sleep(1)
        cartas.remove(sorteio[i])
    if sum(pontos) < 21:
        pedir = float(input(f' \n A sua pontuação é {sum(pontos)}. Digite 1 se deseja pedir outra carta \n Ou digite 2 para proseguir com a pontuação atual: \n '))
        if pedir == 1:
            sorteio = sorteio + list(random.sample(cartas, 1))
            if sorteio[3] in ases:
                if sum(pontos) < 11:
                    print(carta_as)
                    pontos.append(11)
                else:
                    print(carta_as)
                    pontos.append(1)
            if sorteio[3] in dois:
                print(carta_dois)
                pontos.append(2)
            if sorteio[3] in tres:
                print(carta_tres)
                pontos.append(3)
            if sorteio[3] in quatro:
                print(carta_quatro)
                pontos.append(4)
            if sorteio[3] in cinco:
                print(carta_cinco)
                pontos.append(5)
            if sorteio[3] in seis:
                print(carta_seis)
                pontos.append(6)
            if sorteio[3] in sete:
                print(carta_sete)
                pontos.append(7)
            if sorteio[3] in oito:
                print(carta_oito)
                pontos.append(8)
            if sorteio[3] in nove:
                print(carta_nove)
                pontos.append(9)
            if sorteio[3] in dez:
                print(carta_dez)
                pontos.append(10)
            if sorteio[3] in valete:
                print(carta_valete)
                pontos.append(10)
            if sorteio[3] in damas:
                print(carta_dama)
                pontos.append(10)
            if sorteio[3] in reis:
                print(carta_reis)
                pontos.append(10)
            time.sleep(1)
            cartas.remove(sorteio[3])
 #       else:
 #           continue
        if sum(pontos) < 21 and pedir == 1:
            pedir2 = float(input(f' \n A sua pontuação é {sum(pontos)}. Digite 1 se deseja pedir outra carta \n Ou digite 2 para proseguir com a pontuação atual: \n '))
            if pedir2 == 1:
                sorteio = sorteio + list(random.sample(cartas, 1))
                if sorteio[4] in ases:
                    if sum(pontos) < 11:
                        print(carta_as)
                        pontos.append(11)
                    else:
                        print(carta_as)
                        pontos.append(1)
                if sorteio[4] in dois:
                    print(carta_dois)
                    pontos.append(2)
                if sorteio[4] in tres:
                    print(carta_tres)
                    pontos.append(3)
                if sorteio[4] in quatro:
                    print(carta_quatro)
                    pontos.append(4)
                if sorteio[4] in cinco:
                    print(carta_cinco)
                    pontos.append(5)
                if sorteio[4] in seis:
                    print(carta_seis)
                    pontos.append(6)
                if sorteio[4] in sete:
                    print(carta_sete)
                    pontos.append(7)
                if sorteio[4] in oito:
                    print(carta_oito)
                    pontos.append(8)
                if sorteio[4] in nove:
                    print(carta_nove)
                    pontos.append(9)
                if sorteio[4] in dez:
                    print(carta_dez)
                    pontos.append(10)
                if sorteio[4] in valete:
                    print(carta_valete)
                    pontos.append(10)
                if sorteio[4] in damas:
                    print(carta_dama)
                    pontos.append(10)
                if sorteio[4] in reis:
                    print(carta_reis)
                    pontos.append(10)
                time.sleep(1)
                cartas.remove(sorteio[4])

    if sum(pontos) < 21 and pedir == 1 and pedir2 ==1:
        pedir3 = float(input(f' \n A sua pontuação é {sum(pontos)}. Digite 1 se deseja pedir outra carta \n Ou digite 2 para proseguir com a pontuação atual: \n '))
        if pedir3 == 1:
            sorteio = sorteio + list(random.sample(cartas, 1))
            if sorteio[5] in ases:
                if sum(pontos) < 11:
                    print(carta_as)
                    pontos.append(11)
                else:
                    print(carta_as)
                    pontos.append(1)
            if sorteio[5] in dois:
                print(carta_dois)
                pontos.append(2)
            if sorteio[5] in tres:
                print(carta_tres)
                pontos.append(3)
            if sorteio[5] in quatro:
                print(carta_quatro)
                pontos.append(4)
            if sorteio[5] in cinco:
                print(carta_cinco)
                pontos.append(5)
            if sorteio[5] in seis:
                print(carta_seis)
                pontos.append(6)
            if sorteio[5] in sete:
                print(carta_sete)
                pontos.append(7)
            if sorteio[5] in oito:
                print(carta_oito)
                pontos.append(8)
            if sorteio[5] in nove:
                print(carta_nove)
                pontos.append(9)
            if sorteio[5] in dez:
                print(carta_dez)
                pontos.append(10)
            if sorteio[5] in valete:
                print(carta_valete)
                pontos.append(10)
            if sorteio[5] in damas:
                print(carta_dama)
                pontos.append(10)
            if sorteio[5] in reis:
                print(carta_reis)
                pontos.append(10)
            time.sleep(1)
            cartas.remove(sorteio[5])



    print(f' \n A sua pontuação atual é {sum(pontos)} pontos.\n -----Aguarde as cartas da banca ----- \n')
# Programando os pontos da banca!
    time.sleep(2)
    print('\n ------------------------ \n As cartas da banca são: \n ------------------------ \n')
    sorteio_banca = list(random.sample(cartas, 3))
    for i in range(0, 3):
        if sorteio_banca[i] in ases:
            if sum(pontos) < 11:
                print(carta_as)
                pontos_banca.append(11)
            else:
                print(carta_as)
                pontos_banca.append(1)
        elif sorteio_banca[i] in dois:
            print(carta_dois)
            pontos_banca.append(2)
        elif sorteio_banca[i] in tres:
            print(carta_tres)
            pontos_banca.append(3)
        elif sorteio_banca[i] in quatro:
            print(carta_quatro)
            pontos_banca.append(4)
        elif sorteio_banca[i] in cinco:
            print(carta_cinco)
            pontos_banca.append(5)
        elif sorteio_banca[i] in seis:
            print(carta_seis)
            pontos_banca.append(6)
        elif sorteio_banca[i] in sete:
            print(carta_sete)
            pontos_banca.append(7)
        elif sorteio_banca[i] in oito:
            print(carta_oito)
            pontos_banca.append(8)
        elif sorteio_banca[i] in nove:
            print(carta_nove)
            pontos_banca.append(9)
        elif sorteio_banca[i] in dez:
            print(carta_dez)
            pontos_banca.append(10)
        elif sorteio_banca[i] in valete:
            print(carta_valete)
            pontos_banca.append(10)
        elif sorteio_banca[i] in damas:
            print(carta_dama)
            pontos_banca.append(10)
        else:
            print(carta_reis)
            pontos_banca.append(10)
        time.sleep(1)
        cartas.remove(sorteio_banca[i])
    if sum(pontos_banca) < 17 and sum(pontos) <= 21 and sum(pontos) > 16:
        print('\n ---------------------------- \n A banca vai pedir outra carta \n ---------------------------- \n')
        sorteio_banca = sorteio_banca + list(random.sample(cartas, 1))
        if sorteio_banca[3] in ases:
            if sum(pontos_banca) < 11:
                print(carta_as)
                pontos_banca.append(11)
            else:
                print(carta_as)
                pontos_banca.append(1)
        elif sorteio_banca[3] in dois:
            print(carta_dois)
            pontos_banca.append(2)
        elif sorteio_banca[3] in tres:
            print(carta_tres)
            pontos_banca.append(3)
        elif sorteio_banca[3] in quatro:
            print(carta_quatro)
            pontos_banca.append(4)
        elif sorteio_banca[3] in cinco:
            print(carta_cinco)
            pontos_banca.append(5)
        elif sorteio_banca[3] in seis:
            print(carta_seis)
            pontos_banca.append(6)
        elif sorteio_banca[3] in sete:
            print(carta_sete)
            pontos_banca.append(7)
        elif sorteio_banca[3] in oito:
            print(carta_oito)
            pontos_banca.append(8)
        elif sorteio_banca[3] in nove:
            print(carta_nove)
            pontos_banca.append(9)
        elif sorteio_banca[3] in dez:
            print(carta_dez)
            pontos_banca.append(10)
        elif sorteio_banca[3] in valete:
            print(carta_valete)
            pontos_banca.append(10)
        elif sorteio_banca[3] in damas:
            print(carta_dama)
            pontos_banca.append(10)
        else:
            print(carta_reis)
            pontos_banca.append(10)
        time.sleep(1)
        cartas.remove(sorteio_banca[3])
    if sum(pontos_banca) < 17 and sum(pontos) <= 21 and sum(pontos) > 16:
        print('\n ---------------------------- \n A banca vai pedir outra carta \n ---------------------------- \n')
        sorteio_banca = sorteio_banca + list(random.sample(cartas, 1))
        if sorteio_banca[4] in ases:
            if sum(pontos_banca) < 11:
                print(carta_as)
                pontos_banca.append(11)
            else:
                print(carta_as)
                pontos_banca.append(1)
        elif sorteio_banca[4] in dois:
            print(carta_dois)
            pontos_banca.append(2)
        elif sorteio_banca[4] in tres:
            print(carta_tres)
            pontos_banca.append(3)
        elif sorteio_banca[4] in quatro:
            print(carta_quatro)
            pontos_banca.append(4)
        elif sorteio_banca[4] in cinco:
            print(carta_cinco)
            pontos_banca.append(5)
        elif sorteio_banca[4] in seis:
            print(carta_seis)
            pontos_banca.append(6)
        elif sorteio_banca[4] in sete:
            print(carta_sete)
            pontos_banca.append(7)
        elif sorteio_banca[4] in oito:
            print(carta_oito)
            pontos_banca.append(8)
        elif sorteio_banca[4] in nove:
            print(carta_nove)
            pontos_banca.append(9)
        elif sorteio_banca[4] in dez:
            print(carta_dez)
            pontos_banca.append(10)
        elif sorteio_banca[4] in valete:
            print(carta_valete)
            pontos_banca.append(10)
        elif sorteio_banca[4] in damas:
            print(carta_dama)
            pontos_banca.append(10)
        else:
            print(carta_reis)
            pontos_banca.append(10)
        time.sleep(1)
        cartas.remove(sorteio_banca[4])
    if sum(pontos_banca) < 17 and sum(pontos) <= 21 and sum(pontos) > 16:
        print('\n ---------------------------- \n A banca vai pedir outra carta \n ---------------------------- \n')
        sorteio_banca = sorteio_banca + list(random.sample(cartas, 1))
        if sorteio_banca[5] in ases:
            if sum(pontos_banca) < 11:
                print(carta_as)
                pontos_banca.append(11)
            else:
                print(carta_as)
                pontos_banca.append(1)
        elif sorteio_banca[5] in dois:
            print(carta_dois)
            pontos_banca.append(2)
        elif sorteio_banca[5] in tres:
            print(carta_tres)
            pontos_banca.append(3)
        elif sorteio_banca[5] in quatro:
            print(carta_quatro)
            pontos_banca.append(4)
        elif sorteio_banca[5] in cinco:
            print(carta_cinco)
            pontos_banca.append(5)
        elif sorteio_banca[5] in seis:
            print(carta_seis)
            pontos_banca.append(6)
        elif sorteio_banca[5] in sete:
            print(carta_sete)
            pontos_banca.append(7)
        elif sorteio_banca[5] in oito:
            print(carta_oito)
            pontos_banca.append(8)
        elif sorteio_banca[5] in nove:
            print(carta_nove)
            pontos_banca.append(9)
        elif sorteio_banca[5] in dez:
            print(carta_dez)
            pontos_banca.append(10)
        elif sorteio_banca[5] in valete:
            print(carta_valete)
            pontos_banca.append(10)
        elif sorteio_banca[5] in damas:
            print(carta_dama)
            pontos_banca.append(10)
        else:
            print(carta_reis)
            pontos_banca.append(10)
        time.sleep(1)
        cartas.remove(sorteio_banca[5])
    print(f' \n A sua pontuação é {sum(pontos)} e a pontuação da banca é {sum(pontos_banca)} \n')
    if sum(pontos_banca) == 21 and sum(pontos) != 21:
        print(f'Você perdeu {aposta} reais!')
        ganhou = 0
    if sum(pontos_banca) < 21 and sum(pontos) == 21:
        print(f'Você ganhou {aposta} reais!')
        ganhou = 1
    if sum(pontos) < 22 and sum(pontos_banca) > 21:
        ganhou = 1
        print(f'Você ganhou {aposta} reais!')
    if sum(pontos_banca) < 22 and sum(pontos) > 21:
        ganhou = 0
        print(f'Você perdeu {aposta} reais!')
    if sum(pontos) == 21 and sum(pontos_banca) > 21:
        ganhou = 1
        print(f'Você ganhou {aposta} reais!')
    if sum(pontos) > 21 and sum(pontos_banca) > 21:
        print(f'Ninguém ganhou. Os {aposta} reais retornaram para a sua carteira!')
        ganhou = 2
    if sum(pontos) == 21 and sum(pontos_banca) == 21:
        ganhou = 2
        print(f'Ninguém ganhou. Os {aposta} reais retornaram para a sua carteira!')
    if sum(pontos) < 21 and sum(pontos_banca) < 21 and sum(pontos) > sum(pontos_banca):
        ganhou = 1
        print(f'Você ganhou {aposta} reais!')
    if sum(pontos) < 21 and sum(pontos_banca) < 21 and sum(pontos) < sum(pontos_banca):
        ganhou = 0
        print(f'Você perdeu {aposta} reais!')
    continuar = float(input('\n Digite 1 se deseja continuar ou digite 2 para encerrar: \n'))
    if continuar == 1:
        pontos_banca = []
        pontos = []
        n +=1
        print(f' \n ------------------------ \n ------  RODADA  {n +1} ------ \n ------------------------ \n')
        if ganhou ==1:
            inicio += aposta
        elif ganhou==0:
            inicio -= aposta
        else:
            inicio = inicio + 0
        print(f'\n O seu saldo é de {inicio} reais \n')
        if inicio == 0:
            print('Você perdeu tudo!')
            break
    else:
        break